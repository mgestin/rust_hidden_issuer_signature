| :exclamation:  This work is a benchmarking implementation and should not be used in production  |
|-------------------------------------------------------------------------------------------------|

# rust_hidden_issuer_signature

Proof of concept of the Hidden Issuer Anonymous Credential (HIAC) paper. This implementation is based on rust. It uses the BLS12-381 elliptic curve, along with a type-3 pairing. Implementation of the curve is due to zkcrypto group (https://github.com/zkcrypto/bls12_381).

## The need for an hidden issuer anonymous credential

An anonymous credential scheme requires for the Verifier to know and to trust the identity of the issuer of a credential. Indeed, the verifier need to use the issuer public key to verify a credential.

This behaviour can leak extra information about the user (place of residence, weak collusion resistance, ...).

This implementation is a POC of a new Anonymous Credential scheme that addresses these issues, by producing an anonymous credential that hides the issuer of the credential in the set of the verifier's trusted issuers.

# How to use this Library

This library is a POC, and a benchmarking implementation. Each file in ./src implements a specific tool for the whole signature.
The major file is signature.rs, which concatenates all other files, and which implements the whole signature.
ps_sig.rs is the implementation of the well known Pointcheval Sanders signature scheme. It is presented here for comparison.

To build the project, simply install rust (https://doc.rust-lang.org/book/ch01-01-installation.html), and then run "cargo build --release".

The executable file is "./target/release/hidden_issuer".

## How to see documentation

run "cargo doc --open".

## License

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
