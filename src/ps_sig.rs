//! Implementation of the Pointcheval Sanders Signature Scheme.
#![allow(dead_code)]
extern crate bls12_381;
extern crate sha2;

use bls12_381::{pairing, G1Affine, G1Projective, G2Affine, G2Projective, Scalar};

use sha2::{Digest, Sha256};

// We use the generators defined in the library bls12_381.
#[derive(Debug)]
pub struct PSSecretKey {
    x: Scalar,
    y: Scalar,
}

#[derive(Debug)]
pub struct PSPublickey {
    x: G2Projective,
    y: G2Projective,
}

impl PSSecretKey {
    fn new() -> PSSecretKey {
        PSSecretKey {
            x: random_scalar(),
            y: random_scalar(),
        }
    }

    fn gen_public_key(&self) -> PSPublickey {
        PSPublickey {
            x: G2Affine::generator() * self.x,
            y: G2Affine::generator() * self.y,
        }
    }
}

/// An actual PS signature object
pub struct PSSig<'a> {
    sig1: G1Projective,
    sig2: G1Projective,
    message: Message<'a>,
}

impl PSSig<'_> {
    fn new(message: Message, sk: PSSecretKey) -> PSSig {
        let g1 = G1Affine::generator();
        let sig1 = g1 * random_scalar();
        let sig2 = sig1 * (sk.x + message.hash() * sk.y);
        PSSig {
            sig1,
            sig2,
            message,
        }
    }

    fn randomize(&self) -> PSSig{
        let r = random_scalar();
        let sig1 = self.sig1 * r;
        let sig2 = self.sig2 * r;
        PSSig{
            sig1,
            sig2,
            message: self.message.clone(),
        }
    }

    /// Verification process of the PS signature scheme.
    /// Uses the pairing function of bls12_381 crate.
    fn verify(&self, pk: &PSPublickey) -> bool {
        //Case where sig1 is at infinity
        if self.sig1.is_identity().unwrap_u8() == 1 {
            return false;
        }

        let mut tab_g1 = [G1Affine::generator(), G1Affine::generator()];
        let mut tab_g2 = [G2Affine::generator()];

        // From Projective to Affine. Needs to have a result array as mutable argument.
        G1Projective::batch_normalize(&[self.sig1, self.sig2], &mut tab_g1);
        G2Projective::batch_normalize(&[pk.x + (pk.y * self.message.hash())], &mut tab_g2);

        // pairing (Long)
        pairing(&tab_g1[1], &G2Affine::generator()) == pairing(&tab_g1[0], &tab_g2[0])
    }
}
#[derive(Clone)]
pub struct Message<'a> {
    message: &'a str,
    hash: Scalar,
}

impl Message<'_> {
    pub fn hash(&self) -> Scalar {
        self.hash
    }

    pub fn new(message: &str) -> Message {
        Message {
            message,
            hash: Message::from_string_to_scalar(message),
        }
    }
    /// This function uses the SHA256 hash function (128 bit security) to have
    /// a constant size input to use as message.
    fn from_string_to_scalar(message: &str) -> Scalar {
        let mut hasher = Sha256::new();
        hasher.update(message);
        Message::from_hex_to_scalar(&hasher.finalize()[..])
    }

    /// The function from string to scalar is able to take a 32 bytes array to make
    // It of the form of a scalar.
    fn from_hex_to_scalar(hex_tab: &[u8]) -> Scalar {
        let mut final_tab = [0u64, 0u64, 0u64, 0u64];
        // We want to build a scalar. The hash function gives us an array of bytes
        // So we have to convert this array to four u64.

        let mut hex_tab_iter = hex_tab.iter();
        for final_tab_ptr in (0..4).rev() {
            for current_word_ptr in (0..8).rev() {
                match hex_tab_iter.next() {
                    Some(value) => {
                        final_tab[final_tab_ptr] += *value as u64 * 256u64.pow(current_word_ptr)
                    }
                    None => break,
                }
            }
        }
        Scalar::from_raw(final_tab)
    }
}

/// Function that output a random Scalar
pub fn random_scalar() -> Scalar {
    Scalar::from_raw([
        rand::random(),
        rand::random(),
        rand::random(),
        rand::random(),
    ])
}

/// Test when sig1 is at infinity
/// Not in the test module to be able to force infinity for sig1
#[test]
fn unit_sig() {
    let sk = PSSecretKey::new();
    let pk = sk.gen_public_key();

    let mess = Message::new("test");
    let sig = PSSig {
        sig1: G1Projective::identity(),
        sig2: G1Projective::identity(),
        message: mess,
    };
    assert!(!sig.verify(&pk));
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Hash function and conversion test.
    #[test]
    fn test_conversion() {
        let cmp = Scalar::from_raw([
            0xd15d6c15b0f00a08u64,
            0xa3bf4f1b2b0b822cu64,
            0x9a2feaa0c55ad015u64,
            0x9f86d081884c7d65u64,
        ]);
        assert_eq!(Message::new("test").hash(), cmp);
    }

    // Completness test of th PS signature implementation
    #[test]
    fn valid_sig() {
        let sk = PSSecretKey::new();
        let pk = sk.gen_public_key();
        let mess = Message::new("test");
        let sig = PSSig::new(mess, sk);
        assert!(sig.verify(&pk));
    }

    // Test of the PS randomized signature implementation
    #[test]
    fn randomized_sig() {
        let sk = PSSecretKey::new();
        let pk = sk.gen_public_key();
        let mess = Message::new("test");
        let sig = PSSig::new(mess, sk);
        let sig_r = sig.randomize();
        assert!(sig_r.verify(&pk));
    }


    // Soundness test of th PS signature implementation
    #[test]
    fn invalid_sig() {
        let sk = PSSecretKey::new();

        let other_sk = PSSecretKey::new();
        let other_pk = other_sk.gen_public_key();

        let mess = Message::new("test");
        let sig = PSSig::new(mess, sk);
        assert!(!sig.verify(&other_pk));
    }
}

pub mod _bench {
    use super::*;
    use crate::{REV, BenchmarkTime};
    use std::time::Instant;


    /// Mesure all operations in PS signature implementation
    pub fn bench(_bench_time: BenchmarkTime) {
        println!("______________________________________PS Signature Benchmarks________________________________________________");
        keygen_bench();
        signature_bench();
        verification_bench();
        randomized_sig_bench()
    }

    fn keygen_bench() {
        let now = Instant::now();
        for _i in 0..REV {
            // We make REV signature to have a good average
            let sk = PSSecretKey::new();
            let _pk = sk.gen_public_key();
        }
        let elapsed = now.elapsed();
        println!(
            "Total time (keygen, {:.2?} elements) : {:.2?}. Average time (PS keygen) {:.2?}",
            REV,
            elapsed,
            elapsed / REV
        );
    }
    /// Mesure the PS signature process.
    fn signature_bench() {
        let now = Instant::now();
        for _i in 0..REV {
            // We make REV signature to have a good average
            let sk = PSSecretKey::new();
            let _pk = sk.gen_public_key();
            let mess = Message::new("test");
            let _sig = PSSig::new(mess, sk);
        }
        let elapsed = now.elapsed();
        println!(
            "Total time (signature, {:.2?} elements) : {:.2?}. Average time (signature) {:.2?}",
            REV,
            elapsed,
            elapsed / REV
        );
    }

    /// Mesure the verification of a valid signature.
    fn verification_bench() {
        // We begin by creating a valid signature
        let sk = PSSecretKey::new();
        let pk = sk.gen_public_key();
        let mess = Message::new("test");
        let sig = PSSig::new(mess, sk);
        let now = Instant::now();
        for _i in 0..REV {
            sig.verify(&pk);
        }
        let elapsed = now.elapsed();
        println!(
            "Total time (verification, {:.2?} elements) : {:.2?}. Average time (verification) {:.2?}",
            REV,
            elapsed,
            elapsed / REV
        );
    }

    ///Mesure the time to rnadomize one PS signature
    fn randomized_sig_bench() {
        let sk = PSSecretKey::new();
        let _pk = sk.gen_public_key();
        let mess = Message::new("test");
        let sig = PSSig::new(mess, sk);
        let now = Instant::now();
        for _i in 0..REV {
            let _sig_r = sig.randomize();
        }
        let elapsed = now.elapsed();
        println!(
            "Total time (randomization, {:.2?} elements) : {:.2?}. Average time (randomization) {:.2?}",
            REV,
            elapsed,
            elapsed / REV
        );
    }

}
