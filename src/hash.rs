//! Regroups all Hash related functions.
//! Hash primitive used is SHA256
extern crate sha2;
use sha2::{Digest, Sha256};
use bls12_381::Scalar;

/// This function uses the SHA256 hash function (128 bit security) to have
/// a constant size input to use as message.
/// The security of the scheme depends on the validity of this function.
pub fn from_string_to_scalar(message: &str) -> Scalar {
    let mut hasher = Sha256::new();
    hasher.update(message);
    from_hex_to_scalar(&hasher.finalize()[..])
}

/// The function from string to scalar is able to take a 32 bytes array to make
/// it of the form of a scalar.
fn from_hex_to_scalar(hex_tab: &[u8]) -> Scalar {
    let mut final_tab = [0u64, 0u64, 0u64, 0u64];
    // We want to build a scalar. The hash function gives us an array of bytes
    // So we have to convert this array to four u64.
    let mut hex_tab_iter = hex_tab.iter();
    for final_tab_ptr in (0..4).rev() {
        for current_word_ptr in (0..8).rev() {
            match hex_tab_iter.next() {
                Some(value) => {
                    final_tab[final_tab_ptr] += *value as u64 * 256u64.pow(current_word_ptr)
                }
                None => break,
            }
        }
    }
    Scalar::from_raw(final_tab)
}


/// Convert a Scalar to a string ready to be hashed. Output a string of the form "0xA034324...5434A"
pub fn from_scalar_to_string(value : &Scalar) -> String{
    let mut final_str = String::from("0x");
    for item in value.to_bytes().iter().rev() {
        final_str.push_str(&format!("{:#04x}", item)[2..]);
    }
    return final_str;
}
