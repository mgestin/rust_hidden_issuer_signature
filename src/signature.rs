//! Implementation of the Hidden Issuer Signature.
//! HISignature is a struct that does not hide the Issuer.
//! HIBlindSignature hides the Issuer.
extern crate bls12_381;
use crate::keygen::{
    HIVerifierPublicKeyBuild, HIVerifierPublicKeyProd, HIIssuerSecretKey, HIIssuerPublicKey, HIVerifierSecretKey,
};
use crate::message::Message;
use crate::random_scalar;
use crate::ni_zkpop::NI_ZKPoP;
use crate::zkpok::NI_ZKPoK;
use bls12_381::{pairing, G1Affine, G1Projective, G2Affine, G2Projective, Scalar};


/// A signature on a message, computed by an Issuer.
/// The signature is a PS signature, with an extra element, signing the hash of the hash of the message.
pub struct HISignature<'a> {
    message: Message<'a>,
    issuer: HIIssuerPublicKey,
    h1: G1Projective,
    h2: G1Projective,
    sig1: G1Projective,
    sig2: G1Projective,
}

impl<'a> HISignature<'a> {
    /// Constructor of a signature (unrandomized).
    /// Uses the Issuer private key and random points on the curve given by the User.
    /// The exponent of u1 (which is the same as the exponant of u2) and v1 (which is the same as the exponant of v2) must be stored by the user.
    /// the output must be uncommited by the user.
    /// **Non Deterministic**
    pub fn new(
        issuer: &HIIssuerSecretKey,
        message: Message<'a>,
        v1: G1Projective,
        phi: NI_ZKPoP,
    ) -> Option<HISignature<'a>> {
        // Ry (which is the exponants v1) must be saved by the user.
        if phi.verify(){
            let r_i1s = random_scalar();
            let r_i2s = random_scalar();
            let h1 = G1Affine::generator() * r_i1s;
            let h2 = G1Affine::generator() * r_i2s;
            let sig1 = (G1Affine::generator() * issuer.x() + v1 * message.hash()) * r_i1s;
            let sig2 = (G1Affine::generator() * issuer.x() + v1 * message.hashhash()) * r_i2s;

            return Some(HISignature {
                issuer: issuer.generate_public_key(),
                message,
                h1,
                h2,
                sig1,
                sig2,
            });
        }
        else{
            return None;
        }
    }

    /// Uncommits a signature, in order to save it.
    /// **Deterministic**
    pub fn uncommit(&mut self, r: Scalar){
        self.sig1 = self.sig1 - self.h1 * r * self.message.hash();
        self.sig2 = self.sig2 - self.h2 * r * self.message.hashhash();
    }

    /// Verification of the signature before randomization.
    /// **Deterministic**.
    pub fn verify(&self, v2: G2Projective) -> bool {
        // Conversion from projective to affine.
        let mut norm_g1 = [
            G1Affine::identity(),
            G1Affine::identity(),
            G1Affine::identity(),
            G1Affine::identity(),
        ];
        let mut norm_computed_sig = [G2Affine::identity(), G2Affine::identity()];

        G2Projective::batch_normalize(
            &[
                self.issuer.x() + v2 * self.message.hash(),
                self.issuer.x() + v2 * self.message.hashhash(),
            ],
            &mut norm_computed_sig,
        );
        G1Projective::batch_normalize(
            &[
                self.h1.clone(),
                self.h2.clone(),
                self.sig1.clone(),
                self.sig2.clone(),
            ],
            &mut norm_g1,
        );

        if pairing(&norm_g1[2], &G2Affine::generator())
            == pairing(&norm_g1[0], &norm_computed_sig[0])
            && pairing(&norm_g1[3], &G2Affine::generator())
                == pairing(&norm_g1[1], &norm_computed_sig[1])
            && !(self.h1.is_identity().unwrap_u8() == 1)
            && !(self.h2.is_identity().unwrap_u8() == 1)
        {
            return true;
        } else {
            return false;
        }
    }

    /// Randomization process of a signature, with verification of the Verifier's public key.
    /// **Non Deterministic**.
    pub fn verify_and_blind(
        &self,
        verifier: &HIVerifierPublicKeyProd,
        ry: &Scalar,
    ) -> HIBlindSignature {
        assert!(verifier.verify_key(), "Forged Verifier public key.");
        self.blind(verifier, ry, &None, &None)
    }

    /// Randomization process of a signature, without verification of the Verifier's public key.
    /// **Non Deterministic**.
    pub fn blind(
        &self,
        verifier: &HIVerifierPublicKeyProd,
        ry: &Scalar,
        r_ux_val: &Option<Scalar>,
        r_uy_val: &Option<Scalar>,
    ) -> HIBlindSignature {
        //We identify our issuer's public key in the verifier's public key.
        let mut k_opt: Option<usize> = None;
        for (i, item) in verifier.trusted_issuers().iter().enumerate() {
            if *item == self.issuer {
                k_opt = Some(i);
                break;
            }
        }
        let k: usize = k_opt.expect("Specified Issuer not trusted by verifier");
        // Random hiding values
        let r_ux = r_ux_val.unwrap_or(random_scalar());
        let r_uy = r_uy_val.unwrap_or(random_scalar());
        let r_u = random_scalar();
        let r_uu = random_scalar();
        let r_uuu = random_scalar();

        // Preliminaries
        let w_x = verifier.w_x()[k];
        let w_y = verifier.w_y()[k];
        // Building the randomized signature.
        let wx_p = w_x * r_ux;
        let wy_p = w_y * r_uy;
        let x2_p = self.issuer.x() *  r_u;
        let y2_p = self.issuer.y_2() * (ry * r_u);
        let hx_p = G2Affine::generator() * (r_ux * r_u);
        let hy_p = G2Affine::generator() * (ry * r_uy * r_u);
        let h1_p = self.h1 * r_uu;
        let h2_p = self.h2 * r_uuu;
        let sig1_p = self.sig1 * (r_u * r_uu);
        let sig2_p = self.sig2 * (r_u * r_uuu);

        // Conversion from projective to affine. (6) values for G1, 4 for G2
        let mut norm_g1 = [
            G1Affine::identity(),
            G1Affine::identity(),
            G1Affine::identity(),
            G1Affine::identity(),
            G1Affine::identity(),
            G1Affine::identity(),
        ];
        let mut norm_g2 = [
            G2Affine::identity(),
            G2Affine::identity(),
            G2Affine::identity(),
            G2Affine::identity(),
        ];

        G1Projective::batch_normalize(&[h1_p, h2_p, sig1_p, sig2_p, wx_p, wy_p], &mut norm_g1);
        G2Projective::batch_normalize(&[x2_p, y2_p, hx_p, hy_p], &mut norm_g2);

        // Assignation of the variables.
        HIBlindSignature {
            message: self.message,
            h1_p: norm_g1[0],
            h2_p: norm_g1[1],
            sig1_p: norm_g1[2],
            sig2_p: norm_g1[3],
            wx_p: norm_g1[4],
            wy_p: norm_g1[5],
            x2_p: norm_g2[0],
            y2_p: norm_g2[1],
            hx_p: norm_g2[2],
            hy_p: norm_g2[3],
        }
    }


}

/// Randomized Signature.
/// This signature must be verified by the associated Verifier private key, and do not reveal
/// the identity of the Issuer of the original signature.
pub struct HIBlindSignature<'a> {
    message: Message<'a>,
    h1_p: G1Affine,
    h2_p: G1Affine,
    sig1_p: G1Affine,
    sig2_p: G1Affine,
    wx_p: G1Affine,
    wy_p: G1Affine,
    x2_p: G2Affine,
    y2_p: G2Affine,
    hx_p: G2Affine,
    hy_p: G2Affine,
}

impl HIBlindSignature<'_> {
    /// **wy_p** accessor.
    pub fn wy_p(&self) -> &G1Affine {
        &self.wy_p
    }

    /// **wx_p** accessor.
    pub fn wx_p(&self) -> &G1Affine {
        &self.wx_p
    }

    /// Verification of a randomized signature, using the verifier's secret key.
    /// **Deterministic**
    fn verify(&self, sk: &HIVerifierSecretKey) -> bool {
        // Conversion
        let mut norm_g1 = [G1Affine::identity(), G1Affine::identity()];

        G1Projective::batch_normalize(&[sk.acc_s_x().clone(), sk.acc_s_y().clone()], &mut norm_g1);

        let mut norm_g2 = [G2Affine::identity(), G2Affine::identity()];

        G2Projective::batch_normalize(
            &[
                self.x2_p + (self.y2_p * self.message.hash()),
                self.x2_p + (self.y2_p * self.message.hashhash()),
            ],
            &mut norm_g2,
        );

        if !(self.sig1_p.is_identity().unwrap_u8() == 1)
            && !(self.sig2_p.is_identity().unwrap_u8() == 1)
            && !(self.h1_p.is_identity().unwrap_u8() == 1)
            && !(self.h2_p.is_identity().unwrap_u8() == 1)
            && pairing(&self.wx_p, &self.x2_p) == pairing(&norm_g1[0], &self.hx_p)
            && pairing(&self.wy_p, &self.y2_p) == pairing(&norm_g1[1], &self.hy_p)
            && pairing(&self.h1_p, &norm_g2[0]) == pairing(&self.sig1_p, &G2Affine::generator())
            && pairing(&self.h2_p, &norm_g2[1]) == pairing(&self.sig2_p, &G2Affine::generator())
            && self.message.verify_hash()
        {
            return true;
        } else {
            return false;
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use crate::K_TEST;

    #[test]
    fn sig_without_blind_test() {
        let message = Message::new("test");
        let issuer = HIIssuerSecretKey::new();
        let r = random_scalar();
        let ry = random_scalar();
        let v1 = G1Affine::generator() * r + issuer.generate_public_key().y_1() * ry;
        let v2 = issuer.generate_public_key().y_2() * ry;
        let phi = NI_ZKPoP::new(&ry, &r, issuer.generate_public_key().y_1().clone(), G1Affine::generator().clone() * Scalar::one());
        let mut sig = HISignature::new(&issuer, message, v1, phi).expect("invalid ZKPoP");
        sig.uncommit(r);
        assert!(sig.verify(v2));
    }

    #[test]
    fn sig_with_blind_test() {
        // Issuer and signature
        let message = Message::new("test");
        // Verifier keys generation
        let mut issuers_sk: Vec<HIIssuerSecretKey> = Vec::new();
        let mut issuers_pk: Vec<HIIssuerPublicKey> = Vec::new();
        for i in 0..K_TEST {
            issuers_sk.push(HIIssuerSecretKey::new());
            issuers_pk.push(issuers_sk[i].generate_public_key())
        }
        let r = random_scalar();
        let ry = random_scalar();
        let v1 = G1Affine::generator() * r + issuers_sk[0].generate_public_key().y_1() * ry;
        let v2 = issuers_sk[0].generate_public_key().y_2() * ry;

        let phi = NI_ZKPoP::new(&ry, &r, issuers_pk[0].y_1().clone(), G1Affine::generator().clone() * Scalar::one());
        let mut sig = HISignature::new(&issuers_sk[0], message, v1, phi).expect("invalid ZKPoP");
        sig.uncommit(r);


        let mut verifier_pk_build = HIVerifierPublicKeyBuild::new(issuers_pk);

        let verifier_key_pair = verifier_pk_build.generate_private_key();
        let verifier_pk = verifier_key_pair.pk();
        let verifier_sk = verifier_key_pair.sk();
        // Blinded signature for the verifier
        let blind_sig = sig.blind(&verifier_pk, &ry, &None, &None);

        assert!(blind_sig.verify(&verifier_sk));
    }
}

pub mod _bench {
    use super::*;
    use crate::{BenchmarkTime, K_BENCH, REV};
    use std::time::{Instant, Duration};
    use std::convert::TryInto;

    /// Mesure all operations in the keys signature implementation
    pub fn bench(bench_time: BenchmarkTime) {
        println!("______________________________________Signature Benchmarks________________________________________________");
        signature_generation_bench();
        verification_unrandomized_bench();
        blind_bench();
        verification_blinded_bench();
        if let BenchmarkTime::Long = bench_time{
            for i in (5..150).step_by(5){
                verification_blinded_new_key_gen_bench(i as usize);
            }
        }
        verification_blinded_static_gen_bench();
    }

    /// Mesures the signature algorithm.
    fn signature_generation_bench() {
        let mut mess_arr: Vec<Message> = Vec::new();
        let mut v1_arr : Vec<G1Projective> = Vec::new();
        let mut issuer_arr : Vec<HIIssuerSecretKey> = Vec::new();
        let mut phi_arr : Vec<NI_ZKPoP> = Vec::new();
        let mut elapsed_user : Duration = Duration::new(0,0);
        let mut r_arr : Vec<Scalar> = Vec::new();
        for i in 0..REV {
            let message = Message::new("test");
            let issuer = HIIssuerSecretKey::new();
            let issuer_public_key = issuer.generate_public_key();

            // Begins user benchmark
            let now_user = Instant::now();

            r_arr.push(random_scalar());
            let ry = random_scalar();
            let v1 = G1Affine::generator() * r_arr[i as usize] + issuer_public_key.y_1() * ry;
            let _v2 =  issuer.generate_public_key().y_2() * ry;
            let phi = NI_ZKPoP::new(&ry, &r_arr[i as usize],issuer_public_key.y_1().clone(), G1Affine::generator().clone() * Scalar::one());

            elapsed_user += now_user.elapsed();

            mess_arr.push(message);
            issuer_arr.push(issuer);
            v1_arr.push(v1);
            phi_arr.push(phi);
        }
        let mut sig_arr : Vec<HISignature> = Vec::new();
        let now = Instant::now();

        for _ in 0..REV {
            sig_arr.push(HISignature::new(&issuer_arr.pop().unwrap(), mess_arr.pop().unwrap(), v1_arr.pop().unwrap(), phi_arr.pop().unwrap()).expect("invalid ZKPoP"));
        }
        let elapsed = now.elapsed();

        let now_user = Instant::now();
        for i in 0..REV {
            sig_arr[i as usize].uncommit(r_arr[i as usize]);
        }
        elapsed_user += now_user.elapsed();

        println!(
            "Total time (signature generation, {:.2?} elements) : {:.2?}. Average time (signature generation) {:.2?}\n Total time (User side, {:2?} elements):   {:.2?}. Average time (User side) {:.2?}",
            REV,
            elapsed,
            elapsed / REV,
            REV,
            elapsed_user,
            elapsed_user / (2*REV)
        );
    }

    /// Mesures the verification algorithm with an unblinded signature.
    fn verification_unrandomized_bench() {
        let mut sig_arr: Vec<HISignature> = Vec::new();
        let mut v2_arr : Vec<G2Projective> = Vec::new();
        for _ in 0..REV {
            let message = Message::new("test");
            let issuer = HIIssuerSecretKey::new();
            let r = random_scalar();
            let ry = random_scalar();
            let v1 = G1Affine::generator() * r + issuer.generate_public_key().y_1() * ry;
            let v2 = issuer.generate_public_key().y_2() * ry;
            let phi = NI_ZKPoP::new(&ry, &r, issuer.generate_public_key().y_1().clone(), G1Affine::generator().clone() * Scalar::one());
            let mut _sig = HISignature::new(&issuer, message, v1, phi).expect("invalid ZKPoP");
            _sig.uncommit(r);

            sig_arr.push(_sig);
            v2_arr.push(v2)
        }
        let now = Instant::now();
        for i in 0..REV {
            sig_arr[i as usize].verify(v2_arr[i as usize]);
        }
        let elapsed = now.elapsed();
        println!(
            "Total time (verification unblinded, {:.2?} elements) : {:.2?}. Average time (verification unblinded) {:.2?}",
            REV,
            elapsed,
            elapsed / REV
        );
    }

    /// Mesures the blinding algorithm.
    fn blind_bench() {
        let mut sig_arr: Vec<HISignature> = Vec::new();
        let mut ry_arr: Vec<Scalar> = Vec::new();
        let mut issuers_sk: Vec<HIIssuerSecretKey> = Vec::new();
        let mut issuers_pk: Vec<HIIssuerPublicKey> = Vec::new();

        // Signature generation
        for i in 0..K_BENCH {
            let message = Message::new("test");
            issuers_sk.push(HIIssuerSecretKey::new());
            issuers_pk.push(issuers_sk[i].generate_public_key());
            let r = random_scalar();
            let ry = random_scalar();
            let v1 = G1Affine::generator() * r + issuers_sk[i as usize].generate_public_key().y_1() * ry;
            let phi = NI_ZKPoP::new(&ry, &r, issuers_sk[i as usize].generate_public_key().y_1().clone(), G1Affine::generator().clone() * Scalar::one());
            let mut _sig = HISignature::new(&issuers_sk[i as usize], message, v1, phi).expect("invalid ZKPoP");
            _sig.uncommit(r);

            sig_arr.push(_sig);
            ry_arr.push(ry)
        }

        // Verifier generation
        let mut verifier_pk_build = HIVerifierPublicKeyBuild::new(issuers_pk);

        let verifier_key_pair = verifier_pk_build.generate_private_key();
        let verifier_pk = verifier_key_pair.pk();

        // Blinded signature for the verifier

        let mut blind_sig_arr: Vec<HIBlindSignature> = Vec::new();

        let now = Instant::now();
        for i in 0..K_BENCH {
            blind_sig_arr.push(sig_arr[i as usize].blind(
                &verifier_pk,
                &ry_arr[i as usize], &None, &None
            ));
        }
        let elapsed = now.elapsed();
        println!(
            "Total time (blind, {:.2?} elements) : {:.2?}. Average time (blind) : {:.2?}",
            K_BENCH,
            elapsed,
            elapsed/K_BENCH.try_into().unwrap(),
        );
    }

    /// Mesures the blinding algorithm.
    fn verification_blinded_bench() {
        let mut sig_arr: Vec<HISignature> = Vec::new();
        let mut ry_arr: Vec<Scalar> = Vec::new();
        let mut issuers_sk: Vec<HIIssuerSecretKey> = Vec::new();
        let mut issuers_pk: Vec<HIIssuerPublicKey> = Vec::new();

        // Signature generation
        for i in 0..K_BENCH {
            let message = Message::new("test");
            issuers_sk.push(HIIssuerSecretKey::new());
            issuers_pk.push(issuers_sk[i].generate_public_key());
            let r = random_scalar();
            let ry = random_scalar();
            let v1 = G1Affine::generator() * r + issuers_sk[i as usize].generate_public_key().y_1() * ry;
            let phi = NI_ZKPoP::new(&ry, &r, issuers_sk[i as usize].generate_public_key().y_1().clone(), G1Affine::generator().clone() * Scalar::one());
            let mut _sig = HISignature::new(&issuers_sk[i as usize], message, v1, phi).expect("invalid ZKPoP");
            _sig.uncommit(r);

            sig_arr.push(_sig);
            ry_arr.push(ry)
        }

        // Verifier generation
        let mut verifier_pk_build = HIVerifierPublicKeyBuild::new(issuers_pk);

        let verifier_key_pair = verifier_pk_build.generate_private_key();
        let verifier_pk = verifier_key_pair.pk;
        let verifier_sk = verifier_key_pair.sk;

        // Blinded signature for the verifier

        let mut blind_sig_arr: Vec<HIBlindSignature> = Vec::new();

        for i in 0..K_BENCH {
            blind_sig_arr.push(sig_arr[i as usize].blind(
                &verifier_pk,
                &ry_arr[i as usize], &None, &None
            ));
        }
        let now = Instant::now();

        for i in 0..K_BENCH {
            blind_sig_arr[i as usize].verify(&verifier_sk);
        }

        let elapsed = now.elapsed();
        println!(
            "Total time (blinded signature verification, {:.2?} elements) : {:.2?}. Average time (blinded signature verification) : {:.2?}",
            K_BENCH,
            elapsed,
            elapsed/K_BENCH.try_into().unwrap(),
        );
    }

    /// Mesures the blinding algorithm without the interactive commitment exchange (a new verifier key is built each time).
    fn verification_blinded_new_key_gen_bench(k_bench: usize) {
        let mut sig_arr: Vec<HISignature> = Vec::new();
        let mut ry_arr: Vec<Scalar> = Vec::new();
        let mut issuers_sk: Vec<HIIssuerSecretKey> = Vec::new();
        let mut issuers_pk: Vec<HIIssuerPublicKey> = Vec::new();

        // Signature generation
        for i in 0..K_BENCH {
            let message = Message::new("test");
            issuers_sk.push(HIIssuerSecretKey::new());
            issuers_pk.push(issuers_sk[i].generate_public_key());
            let r = random_scalar();
            let ry = random_scalar();
            let v1 = G1Affine::generator() * r + issuers_sk[0].generate_public_key().y_1() * ry;
            let phi = NI_ZKPoP::new(&ry, &r, issuers_sk[0].generate_public_key().y_1().clone(), G1Affine::generator().clone() * Scalar::one());
            let mut _sig = HISignature::new(&issuers_sk[0], message, v1, phi).expect("invalid ZKPoP");
            _sig.uncommit(r);

            sig_arr.push(_sig);
            ry_arr.push(ry)
        }

        // Verifier generation
        let mut verifier_pk_build = HIVerifierPublicKeyBuild::new(issuers_pk);

        let verifier_key_pair = verifier_pk_build.generate_private_key();
        let verifier_pk = verifier_key_pair.pk;
        let verifier_sk = verifier_key_pair.sk;

        // Blinded signature for the verifier

        let mut blind_sig_arr: Vec<HIBlindSignature> = Vec::new();

        for i in 0..REV {
            blind_sig_arr.push(sig_arr[i as usize].blind(
                &verifier_pk,
                &ry_arr[i as usize], &None, &None
            ));
        }

        let now = Instant::now();

        for i in 0..REV {
            verifier_pk_build.generate_private_key();
            blind_sig_arr[i as usize].verify(&verifier_sk);
        }
        let elapsed = now.elapsed();
        println!(
            "Total time (blinded signature verification with new verifier key build, {:.2?} elements) : {:.2?}. Average time (blinded signature verification) : {:.2?}",
            k_bench,
            elapsed,
            elapsed/REV.try_into().unwrap(),
        );
    }

    /// Mesures the time taken by the verifier and the user to use the static verifier key (with the commitment reveal exchange which adds 10 exponentiations for the user and 9 for the verifier).
    fn verification_blinded_static_gen_bench() {
        let mut sig_arr: Vec<HISignature> = Vec::new();
        let mut ry_arr: Vec<Scalar> = Vec::new();
        let mut issuers_sk: Vec<HIIssuerSecretKey> = Vec::new();
        let mut issuers_pk: Vec<HIIssuerPublicKey> = Vec::new();

        // Signature generation
        for i in 0..K_BENCH {
            let message = Message::new("test");
            issuers_sk.push(HIIssuerSecretKey::new());
            issuers_pk.push(issuers_sk[i].generate_public_key());
            let r = random_scalar();
            let ry = random_scalar();
            let v1 = G1Affine::generator() * r + issuers_sk[0].generate_public_key().y_1() * ry;
            let phi = NI_ZKPoP::new(&ry, &r, issuers_sk[0].generate_public_key().y_1().clone(), G1Affine::generator().clone() * Scalar::one());
            let mut _sig = HISignature::new(&issuers_sk[0], message, v1, phi).expect("invalid ZKPoP");
            _sig.uncommit(r);

            sig_arr.push(_sig);
            ry_arr.push(ry)
        }

        // Verifier generation
        let mut verifier_pk_build = HIVerifierPublicKeyBuild::new(issuers_pk);

        let verifier_key_pair = verifier_pk_build.generate_private_key();
        let verifier_pk = verifier_key_pair.pk;
        let verifier_sk = verifier_key_pair.sk;

        // Blinded signature for the verifier

        let mut blind_sig_arr: Vec<HIBlindSignature> = Vec::new();

        let r_ux = random_scalar();
        let r_uy = random_scalar();

        // Randomization
        let now_issuer_randomize = Instant::now();
        for i in 0..REV {
            blind_sig_arr.push(sig_arr[i as usize].blind(
                &verifier_pk,
                &ry_arr[i as usize], &Some(r_ux), &Some(r_uy)
            ));
        }
        let elapsed_issuer_randomize = now_issuer_randomize.elapsed();

        let r_a = random_scalar();
        let r_b = random_scalar();
        let r_c = random_scalar();
        let r_d = random_scalar();

        // Commit reveal exchange on W_x
        let mut w_xpp : Vec<G1Projective> = Vec::new();
        let mut w_xp : Vec<G1Projective> = Vec::new();
        let mut c1 : Vec<G1Projective> = Vec::new();
        let mut c2 : Vec<G1Projective> = Vec::new();
        let mut c1_p : Vec<G1Projective> = Vec::new();
        let mut c2_p : Vec<G1Projective> = Vec::new();
        let mut c1_pp : Vec<G1Projective> = Vec::new();
        let mut phi_1_1 : Vec<NI_ZKPoK> = Vec::new();
        let mut phi_1_2 : Vec<NI_ZKPoK> = Vec::new();
        let mut phi_2_1 : Vec<NI_ZKPoK> = Vec::new();
        let mut phi_2_2 : Vec<NI_ZKPoK> = Vec::new();

        let now_0 = Instant::now();

        // commit reveal exchange for user (first part)
        let now_issuer_1 = Instant::now();
        for i in 0..REV {
            w_xpp.push(G1Affine::generator() * (verifier_sk.sk_x() *r_ux) + blind_sig_arr[i as usize].wx_p()   + G1Affine::generator() * r_a);
            c1.push(G1Affine::generator() * (r_ux * r_b));
            c2.push(G1Affine::generator() * (r_a * r_c));
            phi_1_1.push(NI_ZKPoK::new(&(r_ux*r_b), G1Projective::generator())) ;
            phi_1_2.push(NI_ZKPoK::new(&(r_a*r_c), G1Projective::generator())) ;
        }
        let elapsed_issuer_1 = now_issuer_1.elapsed();

        // Commit reveal exchange for verifier (first part)
        let now_1 = Instant::now();
        for i in 0..REV {
            phi_1_1[i as usize].verify();
            phi_1_2[i as usize].verify();
            c1_p.push(c1[i as usize] * (verifier_sk.sk_x() * r_d));
            c2_p.push(c2[i as usize] * r_d);
            phi_2_1.push(NI_ZKPoK::new(&(verifier_sk.sk_x()*r_d), c1[i as usize]));
            phi_2_2.push(NI_ZKPoK::new(&r_d, c2[i as usize]));
        }
        let elapsed_1 = now_1.elapsed();

        // commit reveal exchange part 2
        // user
        let now_issuer_2 = Instant::now();
        for i in 0..REV {
            phi_2_1[i as usize].verify();
            phi_2_2[i as usize].verify();
            c1_pp.push(c1_p[i as usize] * (r_b.invert().unwrap()) + c2_p[i as usize] * (r_c.invert().unwrap()));
        }
        let elapsed_issuer_2 = now_issuer_2.elapsed();

        // Verifier
        let now_2 = Instant::now();
        for i in 0..REV {
            w_xp.push(w_xpp[i as usize] + c1_pp[i as usize] * (-r_d.invert().unwrap()));
            let mut cmp_wx = [G1Affine::identity()];
            G1Projective::batch_normalize(&[w_xp[i as usize]], &mut cmp_wx);
            // println!("{:?}", cmp_wx[0]);
            // println!("{:?}", blind_sig_arr[i as usize].wx_p);
            assert!(cmp_wx[0] == blind_sig_arr[i as usize].wx_p)
        }
        let elapsed_2 = now_2.elapsed();


        // WY _____________________________________________________

        let mut w_ypp : Vec<G1Projective> = Vec::new();
        let mut w_yp : Vec<G1Projective> = Vec::new();
        let mut c1 : Vec<G1Projective> = Vec::new();
        let mut c2 : Vec<G1Projective> = Vec::new();
        let mut c1_p : Vec<G1Projective> = Vec::new();
        let mut c2_p : Vec<G1Projective> = Vec::new();
        let mut c1_pp : Vec<G1Projective> = Vec::new();
        let mut phi_1_1 : Vec<NI_ZKPoK> = Vec::new();
        let mut phi_1_2 : Vec<NI_ZKPoK> = Vec::new();
        let mut phi_2_1 : Vec<NI_ZKPoK> = Vec::new();
        let mut phi_2_2 : Vec<NI_ZKPoK> = Vec::new();
        // PArt 1
        // User
        let now_issuer_3 = Instant::now();
        for i in 0..REV {
            w_ypp.push(G1Affine::generator() * (verifier_sk.sk_y() *r_uy) + blind_sig_arr[i as usize].wy_p()   + G1Affine::generator() * r_a);
            c1.push(G1Affine::generator() * (r_uy * r_b));
            c2.push(G1Affine::generator() * (r_a * r_c));
            phi_1_1.push(NI_ZKPoK::new(&(r_uy*r_b), G1Projective::generator())) ;
            phi_1_2.push(NI_ZKPoK::new(&(r_a*r_c), G1Projective::generator())) ;
        }
        let elapsed_issuer_3 = now_issuer_3.elapsed();
        // Verifier
        let now_3 = Instant::now();
        for i in 0..REV {
            phi_1_1[i as usize].verify();
            phi_1_2[i as usize].verify();
            c1_p.push(c1[i as usize] * (verifier_sk.sk_y() * r_d));
            c2_p.push(c2[i as usize] * r_d);
            phi_2_1.push(NI_ZKPoK::new(&(verifier_sk.sk_y()*r_d), c1[i as usize]));
            phi_2_2.push(NI_ZKPoK::new(&r_d, c2[i as usize]));
        }
        let elapsed_3 = now_3.elapsed();

        // Part 2
        // User
        let now_issuer_4 = Instant::now();
        for i in 0..REV {
            phi_2_1[i as usize].verify();
            phi_2_2[i as usize].verify();
            c1_pp.push(c1_p[i as usize] * (r_b.invert().unwrap()) + c2_p[i as usize] * (r_c.invert().unwrap()));
        }
        let elapsed_issuer_4 = now_issuer_4.elapsed();

        // Verifier
        let now_4 = Instant::now();
        for i in 0..REV {
            w_yp.push(w_ypp[i as usize] + c1_pp[i as usize] * (-r_d.invert().unwrap()));
            let mut cmp_wy = [G1Affine::identity()];
            G1Projective::batch_normalize(&[w_yp[i as usize]], &mut cmp_wy);
            // println!("{:?}", cmp_wx[0]);
            // println!("{:?}", blind_sig_arr[i as usize].wx_p);
            assert!(cmp_wy[0] == blind_sig_arr[i as usize].wy_p)
        }
        let elapsed_4 = now_4.elapsed();

        // WY END ___________________________________________________

        // Verify
        let now_5 = Instant::now();
        for i in 0..REV {
            blind_sig_arr[i as usize].verify(&verifier_sk);
        }
        let elapsed_5 = now_5.elapsed();
        let elapsed_0 = now_0.elapsed();
        let total_elapsed = elapsed_1 + elapsed_2 + elapsed_3 + elapsed_4 + elapsed_5;
        let total_elapsed_user = elapsed_issuer_1 + elapsed_issuer_2 + elapsed_issuer_3 + elapsed_issuer_4 +elapsed_issuer_randomize;
        println!(
            "Total time (blinded signature verification with commit reveal exchange, {:.2?} elements) : {:.2?}. Average time (blinded signature verification) : {:.2?}. Average time (with user time taken into account): {:.2?}",
            K_BENCH,
            total_elapsed,
            total_elapsed/REV.try_into().unwrap(),
            elapsed_0/REV.try_into().unwrap()
        );
        println!(
            "Total time (commit reveal exchange for the user with randomization, {:.2?} elements) : {:.2?}. Average time (commit reveal exchange for the user) : {:.2?}",
            K_BENCH,
            total_elapsed_user,
            total_elapsed_user/REV.try_into().unwrap(),
        );
        println!(
            "Total time (issuer randomization, {:.2?} elements) : {:.2?}. Average time (issuer randomization) : {:.2?}",
            K_BENCH,
            elapsed_issuer_randomize,
            elapsed_issuer_randomize/REV.try_into().unwrap(),
        );
    }
}
