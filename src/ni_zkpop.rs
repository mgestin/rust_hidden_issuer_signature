//! Implementation of Non Interactive Zero Knowledge Proof of Pedersenn commitment..
use super::hash::from_string_to_scalar;
use super::*;
use bls12_381::G1Projective;

#[allow(non_camel_case_types)]
/// A NI_ZKPoP where y_1 = g_1^alpha. This struct is the struct meant to be send to the verifier,
/// thus it does not contain any secret value.
pub struct NI_ZKPoP {
    g: G1Projective,
    h: G1Projective,
    y: G1Projective,
    z: G1Projective,
    c: Scalar,
    s_alpha: Scalar,
    s_beta: Scalar,
}

impl NI_ZKPoP {
    /// **y** parameter accessor.
    pub fn y(&self) -> &G1Projective {
        return &self.y;
    }
    /// Builds a new Non Interactive Proof of Knowledge, with two given elements g and h and two given secret number alpha and beta.
    pub fn new(alpha: &Scalar, beta: &Scalar, g: G1Projective, h: G1Projective) -> NI_ZKPoP {
        // Random commitment values
        let r_alpha = random_scalar();
        let r_beta = random_scalar();
        // Commitment
        let z= g * r_alpha + h * r_beta;
        // Final value
        let y = g * alpha + h * beta;
        // We use debug layout as hash entries for strucs object for convienience
        // A production implementation should use a form not dependant on the objects,
        // for example a string "0x1AF34234...982".
        let c = from_string_to_scalar(
            &(format!("{:?}", g) + &format!("{:?}", h) + &format!("{:?}", y) + &format!("{:?}", z)),
        );
        let s_alpha = r_alpha - (c * alpha);
        let s_beta = r_beta - (c * beta);
        NI_ZKPoP {
            g,
            h,
            y,
            z,
            c,
            s_alpha,
            s_beta,
        }
    }

    /// Verification of the ZKP
    pub fn verify(&self) -> bool {
        // verifivation of the hash
        if self.c
            != from_string_to_scalar(
                &(format!("{:?}", self.g) + &format!("{:?}", self.h) + &format!("{:?}", self.y) + &format!("{:?}", self.z)),
            )
        {
            return false;
        }


        if !(self.y * self.c + self.g * self.s_alpha + self.h * self.s_beta == self.z) {
            return false;
        }


        return true;
    }
}


// The goal of this test is to validate that our ni-zkpop is able to reject an invalid ZKP
// This function must be outside of the test module, to buil a ZKP without the constructor
#[test]
fn invalid_zkp() {
    use crate::K_TEST;
    use bls12_381::G1Affine;

    let k = K_TEST;
    let g = &G1Affine::generator() * random_scalar();
    let h = &G1Affine::generator() * random_scalar();
    let alpha = random_scalar();
    let beta = random_scalar();
    let r_alpha = random_scalar();
    let r_beta = random_scalar();
    // Commitment
    let z= g * r_alpha + h * r_beta;
    // Final value
    let y = g * alpha + h * beta;

    let c = from_string_to_scalar(
        &(format!("{:?}", g) + &format!("{:?}", h) + &format!("{:?}", y) + &format!("{:?}", z)),
    );
    let s_alpha = r_alpha - (c * alpha);
    let s_beta = random_scalar();
    let beta = NI_ZKPoP {
        g,
        h,
        y,
        z,
        c,
        s_alpha,
        s_beta,
    };
    assert!(!beta.verify());
}

#[cfg(test)]
mod tests {
    use super::*;
    use bls12_381::G1Affine;

    #[test]
    fn valid_zkpop() {
        let g = G1Affine::generator() * random_scalar();
        let h = G1Affine::generator() * random_scalar();

        let alpha = random_scalar();
        let beta = random_scalar();
        let phi = NI_ZKPoP::new(&alpha, &beta, g, h);
        assert!(phi.verify());
    }
}
