//! Struct to store a message and its hashes.
//! The hashes can be verified, by comparing them to the message.
use super::hash::{from_scalar_to_string, from_string_to_scalar};
use bls12_381::Scalar;

/// Message struct, along with hash of the message, and hash of the hash.
#[derive(Copy, Clone)]
pub struct Message<'a> {
    message: &'a str,
    hash: Scalar,
    hashhash: Scalar,
}

impl Message<'_> {
    /// **message** parameter accessor.
    pub fn message(&self) -> &str{
        &self.message
    }
    /// **hash** parameter accessor.
    pub fn hash(&self) -> &Scalar {
        &self.hash
    }

    /// **hashhash** parameter accessor
    pub fn hashhash(&self) -> &Scalar {
        &self.hashhash
    }

    /// Builds a new message, and the associated hashes.
    pub fn new(message: &str) -> Message {
        let hash = from_string_to_scalar(message);
        let hashhash = from_string_to_scalar(&from_scalar_to_string(&hash));
        Message {
            message,
            hash,
            hashhash,
        }
    }

    /// Verifies the validity of the hashes compared to the message string.
    pub fn verify_hash(&self) -> bool {
        let hash = from_string_to_scalar(self.message);
        let hashhash = from_string_to_scalar(&from_scalar_to_string(&hash));
        if hash == self.hash && hashhash == self.hashhash {
            return true;
        } else {
            return false;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    /// Hash function and conversion test.
    #[test]
    fn test_conversion() {
        let cmp1 = Scalar::from_raw([
            0xd15d6c15b0f00a08u64,
            0xa3bf4f1b2b0b822cu64,
            0x9a2feaa0c55ad015u64,
            0x9f86d081884c7d65u64,
        ]);
        // Hash of "0x2b99292e5eaf001d66f61298bbb8f8105001ab182b0d262dd15d6c16b0f00a07"
        let cmp2 = Scalar::from_raw([
            0x78AD30E2090FFA2Cu64,
            0x377D19F18A9B8FAEu64,
            0x1934FB1E6E012F2Du64,
            0x836493AA820F677Du64,
        ]);
        let mess = Message::new("test");
        assert_eq!(*mess.hash(), cmp1);
        assert_eq!(*mess.hashhash(), cmp2);
    }
}
