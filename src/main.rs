/// This library is only used for benchmark of the signature scheme.
fn main() {
    _benchmark();
}

fn _benchmark(){
    use hidden_issuer::{keygen, signature, ps_sig, BenchmarkTime};

    keygen::_bench::bench(BenchmarkTime::Long);
    signature::_bench::bench(BenchmarkTime::Long);
    ps_sig::_bench::bench(BenchmarkTime::Long);
}
