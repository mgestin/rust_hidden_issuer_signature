//! Implementation of Non Interactive Zero Knowledge Proof of Equality.
use super::hash::from_string_to_scalar;
use super::*;
use bls12_381::G1Projective;

#[allow(non_camel_case_types)]
/// A NI_ZKPoEquality where y_1 = g_1^alpha. This struct is the struct meant to be send to the verifier,
/// thus it does not contain any secret value.
pub struct NI_ZKPoE {
    // Number of equations:
    g: Vec<G1Projective>,
    y: Vec<G1Projective>,
    z: Vec<G1Projective>,
    c: Scalar,
    s_alpha: Scalar,
}

impl NI_ZKPoE {
    /// **y** parameter accessor.
    pub fn y(&self) -> &Vec<G1Projective> {
        return &self.y;
    }
    /// Builds a new Non Interactive Proof of Knowledge of Equality, with a given vector g and a given secret number alpha.
    pub fn new(alpha: &Scalar, g: Vec<G1Projective>) -> NI_ZKPoE {
        // Random commitment values
        let k = g.len();
        let r_alpha = random_scalar();
        let mut z: Vec<G1Projective> = Vec::new();
        let mut y: Vec<G1Projective> = Vec::new();
        for i in 0..k {
            z.push(g[i] * r_alpha);
            y.push(g[i] * alpha);
        }
        // We use debug layout as hash entries for struct objects for convienience.
        // A production implementation should use a form not dependant on the objects,
        // for example a string "0x1AF34234...982".
        let c = from_string_to_scalar(
            &(format!("{:?}", g) + &format!("{:?}", y) + &format!("{:?}", z)),
        );
        let s_alpha = r_alpha - (c * alpha);

        NI_ZKPoE {
            g,
            y,
            z,
            c,
            s_alpha,
        }
    }

    /// Verification of the ZKP
    pub fn verify(&self) -> bool {
        // verifivation of the hash
        if self.c
            != from_string_to_scalar(
                &(format!("{:?}", self.g) + &format!("{:?}", self.y) + &format!("{:?}", self.z)),
            )
        {
            return false;
        }

        // Verification of the completness
        for i in 0..self.g.len() {
            if !(self.y[i] * self.c + self.g[i] * self.s_alpha == self.z[i]) {
                return false;
            }
        }

        return true;
    }
}


/// The goal of this test is to validate that our ni-zkp is able to reject an inequality of the secret factors
/// This function must be outside of the test module, to buil a ZKP without the constructor
#[test]
fn invalid_zkpop() {
    use crate::K_TEST;
    use bls12_381::G1Affine;

    let k = K_TEST;
    let mut g = Vec::new();
    for _ in 0..k {
        g.push(&G1Affine::generator() * random_scalar());
    }
    let alpha = random_scalar();
    let r_alpha = random_scalar();
    let mut z: Vec<G1Projective> = Vec::new();
    let mut y: Vec<G1Projective> = Vec::new();
    for i in 0..(k - 1) {
        z.push(g[i] * r_alpha);
        y.push(g[i] * alpha);
    }
    let fake = random_scalar();

    z.push(g[(k - 1)] * r_alpha);
    y.push(g[(k - 1)] * fake);
    // We use debug layout as hash entries for strucs object for convienience
    let c =
        from_string_to_scalar(&(format!("{:?}", g) + &format!("{:?}", y) + &format!("{:?}", z)));
    let s_alpha = r_alpha - (c * alpha);

    let beta = NI_ZKPoE {
        g,
        y,
        z,
        c,
        s_alpha,
    };
    assert!(!beta.verify());
}

#[cfg(test)]
mod tests {
    use super::*;
    use bls12_381::G1Affine;

    #[test]
    fn valid_zkpop() {
        let mut g = Vec::new();

        for _ in 0..K_TEST {
            g.push(G1Affine::generator() * random_scalar());
        }
        let alpha = random_scalar();
        let beta = NI_ZKPoE::new(&alpha, g);
        assert!(beta.verify());
    }
}
