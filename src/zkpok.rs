//! Implementation of Non Interactive Zero Knowledge Proof of Knowledge.
use super::hash::from_string_to_scalar;
use super::*;
use bls12_381::G1Projective;

#[allow(non_camel_case_types)]
/// A NI_ZKPoEquality where y_1 = g_1^alpha. This struct is the struct meant to be send to the verifier,
/// thus it does not contain any private value.
pub struct NI_ZKPoK {
    // Number of equations:
    g: G1Projective,
    y: G1Projective,
    z: G1Projective,
    c: Scalar,
    s_alpha: Scalar,
}

impl NI_ZKPoK {
    /// **y** parameter accessor.
    pub fn y(&self) -> &G1Projective {
        return &self.y;
    }
    /// Builds a new Non Interactive Proof of Knowledge of Equality, with a given vector g and a given secret number alpha.
    pub fn new(alpha: &Scalar, g: G1Projective) -> NI_ZKPoK {
        // Random commitment values
        let r_alpha = random_scalar();
        let z = g*r_alpha;
        let y = g*alpha;

        // We use debug layout as hash entries for strucs object for convienience
        // A production implementation should use a form not dependant on the objects,
        // for example a string "0x1AF34234...982".
        let c = from_string_to_scalar(
            &(format!("{:?}", g) + &format!("{:?}", y) + &format!("{:?}", z)),
        );
        let s_alpha = r_alpha - (c * alpha);

        NI_ZKPoK {
            g,
            y,
            z,
            c,
            s_alpha,
        }
    }

    /// Verification of the ZKP
    pub fn verify(&self) -> bool {
        // verifivation of the hash
        if self.c
            != from_string_to_scalar(
                &(format!("{:?}", self.g) + &format!("{:?}", self.y) + &format!("{:?}", self.z)),
            )
        {
            return false;
        }

        // Verification of the completness
        if !(self.y * self.c + self.g * self.s_alpha == self.z) {
                return false;

        }

        return true;
    }
}


/// The goal of this test is to validate that our ni-zkp is able to reject an inequality of the secret factors
/// This function must be outside of the test module, to buil a ZKP without the constructor
#[test]
fn invalid_zkpok() {
    use crate::K_TEST;
    use bls12_381::G1Affine;

    let k = K_TEST;
    let g = G1Affine::generator() * random_scalar();
    let alpha = random_scalar();
    let r_alpha = random_scalar();
    let z = g * r_alpha;
    let y = g * alpha;
    let fake = random_scalar();
    // We use debug layout as hash entries for strucs object for convienience
    let c =
        from_string_to_scalar(&(format!("{:?}", g) + &format!("{:?}", y) + &format!("{:?}", z)));
    let s_alpha = r_alpha - (c * fake);

    let beta = NI_ZKPoK {
        g,
        y,
        z,
        c,
        s_alpha,
    };
    assert!(!beta.verify());
}

#[cfg(test)]
mod tests {
    use super::*;
    use bls12_381::G1Affine;

    #[test]
    fn valid_zkpok() {
        let g = G1Affine::generator() * random_scalar();

        let alpha = random_scalar();
        let beta = NI_ZKPoK::new(&alpha, g);
        assert!(beta.verify());
    }
}
